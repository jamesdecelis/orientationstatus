package mt.edu.mcast.orientationstatus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showToast("onCreate");
    }

    private void showToast(String msg){
        Toast t = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        t.show();
    }

    protected void onStart(){
        super.onStart();
        showToast("onStart");
    }

    protected void onStop(){
        super.onStop();
        showToast("onStop");
    }

    protected void onPause(){
        super.onPause();
        showToast("onPause");
    }

    public void onResume(){
        super.onResume();
        showToast("onResume");
    }

    public void onRestart(){
        super.onRestart();
        showToast("onRestart");
    }

    public void onDestroy(){
        super.onDestroy();
        showToast("onDestroy");
    }

}
